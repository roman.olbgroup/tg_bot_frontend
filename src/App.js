import { useState } from "react";
import { LOGIN_URL } from "./const";

const App = () => {
  const [username, setUserame] = useState("");
  const [password, setPassword] = useState("");

  const getUserData = async (username, password) => {
    const data = new FormData();
    data.append("username", username);
    data.append("password", password);
    const res = await fetch(LOGIN_URL, {
      method: "post",
      mode: "no-cors",
      body: data,
    });
    console.log("res", res, "status:", res.status);
  };

  return (
    <div>
      <div>Username:</div>
      <input value={username} onChange={(e) => setUserame(e.target.value)} />
      <div>Password:</div>
      <input
        value={password}
        type="password"
        onChange={(e) => setPassword(e.target.value)}
      />
      <div>
        <button onClick={() => getUserData(username, password)}>Login</button>
      </div>
    </div>
  );
};

export default App;
